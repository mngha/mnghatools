"""
(c) MNG-HA Molecular Oncology Laboratory
"""

from __future__ import print_function
from __future__ import absolute_import


from setuptools import setup, find_packages
exec(open('mnghatools/version.py').read())


def load_requirements():
    f = open('requirements.txt')
    return [l.strip(' ') for l in f]

setup(
    name='mnghatools',
    version=__version__,
    py_modules=['mnghatools'],
    packages=find_packages(),
    include_package_data=True,
    url='https://bitbucket.org/mngha/mnghatools',
    author='Saeed Al Turki',
    author_email='alturkisa@ngha.med.sa',
    license='MIT',
    install_requires=load_requirements(),
    entry_points='''
        [console_scripts]
        mnghatools=mnghatools.main:cli
    ''',
)
