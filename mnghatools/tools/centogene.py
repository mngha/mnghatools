"""
(c) MNG-HA Molecular Oncology Laboratory
"""
from __future__ import print_function
from __future__ import absolute_import

import os
import gzip
from sys import platform as _platform

import click


def open_vcf(path):
    """
    Use gzip if the file is compressed
    :param path: String
    :return: file object
    """
    if path.endswith(".gz"):
        return gzip.open(path, 'r')
    else:
        return open(path, 'r')


def main(vcf_path, vcf_subset_path):
    """
    Parse VCF file to remove non GATK calls / columns and print to screen.
    :param path: Absolute path to the input VCF file
    :return: None
    """
    cmd = None  # use vcf-subset command if vcf is multicaller or just zcat if not
    with open_vcf(vcf_path) as vcf_f:
        for line in vcf_f:
            if line.startswith("#CHROM"):
                line = line.strip().split("\t")
                if len(line) <= 10:
                    # not a multi-caller vcf file
                    zcat = 'gzcat' if _platform == 'darwin' else 'zcat'
                    cmd = '{0} {1}'.format(zcat, vcf_path)
                else:
                    for i in range(10, len(line)):
                        if 'GATK' in line[i]:
                            cmd = '{0} -c {1} {2}'.format(vcf_subset_path, line[i], vcf_path)
        if cmd:
            os.system(cmd)
        else:
            raise 'The VCF file seems like a multi-caller but could not ' \
                  'locate the GATK column (missing from #CHROM line)'


@click.group(invoke_without_command=True)
@click.option('--vcf-path', '-i', required=True, type=click.Path(exists=True),
              help='Path to input VCF file (required)')
@click.option('--vcf-subset-tool', '-s', required=True, type=click.Path(exists=True),
              help='Path to vcf-subset tool (required)')
def cli(vcf_path=None, vcf_subset_tool=None):
    """Remove any non GATK calls"""
    main(vcf_path, vcf_subset_tool)

if __name__ == '__main__':
    cli()
