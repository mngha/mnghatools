"""
(c) MNG-HA Molecular Oncology Laboratory
"""
from __future__ import print_function
from __future__ import absolute_import

import click
import pandas as pd

# These are the names of PED related columns in the manifest file
PED_COLUMNS = ['FAMILY_ID', 'PERSON_ID', 'FATHER_ID', 'MOTHER_ID', 'SEX', 'HEALTH_STATUS']


def main(manifest_path, ped_path):
    """
    Extract sample related info from CIDER's manifest file to make a new PED file to be used with Gemini
    :param manifest_path: Path to manifest file (germline)
    :param ped_path: Path to save PED file
    :return: None
    """
    manifest_df = pd.read_csv(manifest_path, sep='\t', comment="#")
    ped_df = manifest_df[PED_COLUMNS]
    ped_df.to_csv(ped_path, sep='\t', header=False, index=False)


@click.group(invoke_without_command=True)
@click.option('--manifest-path', '-m', required=True, type=click.Path(exists=True),
              help='Path to CIDer\'s manifest file (required)')
@click.option('--ped-path', '-p', help='Path to write PED file')
def cli(manifest_path=None, ped_path=None):
    """Convert CIDer's manifest to PED file"""
    main(manifest_path, ped_path)

if __name__ == '__main__':
    cli()


