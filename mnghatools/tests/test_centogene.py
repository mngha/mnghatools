"""
(c) MNG-HA Molecular Oncology Laboratory
"""
from __future__ import print_function
from __future__ import absolute_import

from nose.tools import assert_equal


from mnghatools.tools import centogene


def test_get_valid_columns_indices():
    """
    Test get_valid_columns_indices method
    """
    # VCF file with single sample / caller column, return list of 10 indices
    header = '#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t62275862_S8'
    assert_equal(centogene.get_valid_columns_indices(header), range(0, 10))

    header = '#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t62275862_S8.FREEBAYES\t62275862_S8.GATK'
    assert_equal(centogene.get_valid_columns_indices(header), range(0, 9) + [10])

    header = '#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t62275862_S8.GATK\t62275862_S8.FREEBAYES'
    assert_equal(centogene.get_valid_columns_indices(header), range(0, 9) + [9])


def test_extract_gatk_format_values():
    """
    Test extract_gatk_format_values method
    """
    sample_values = '0/1:12,49:.:.:99:969,0,140'
    assert_equal(centogene.extract_gatk_format_values(sample_values),
                 ('GT:AD:GQ:PL', '0/1:12,49:99:969,0,140'))
