#!/bin/bash -e
pip install virtualenv
virtualenv-2.7 venv/
source venv/bin/activate

pip install -r requirements.txt
python setup.py develop
