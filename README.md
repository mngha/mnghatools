
Collection of scripts used in MNG-HA Molecular Laboratories pipelines.

 
## Installation ##
```console
git clone git@bitbucket.org:salturki/mnghatools.git
cd mnghatools
bash setup.sh
```

## Usage ##

### Centogene VCF preprocessor ###

```console
mnghatools centogene -i input.vcf.gz | bgzip -c > output.vcf.gz
```
This tool does the following

* check if the VCF file has more than one caller (GATK, FreeBayes, samtools)
* Removes non GATK column (wrongly added as a sample column)
* ignore any variant with missed call in GATK
* writes the output to stdout


###  manifest2ped ###

Extract PED related columns from CIDer's manifest file to be used with Gemini
```console
mnghatools manifest2ped -m file.manifest -p file.ped
```
